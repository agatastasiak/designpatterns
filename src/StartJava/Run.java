import abstractFactory.MainFurnitureFactory;
import abstractFactory.StyledFurnitures;
import abstractFactory.furnitureMainItems.Armchair;
import abstractFactory.furnitureMainItems.Sofa;
import abstractFactory.furnitureMainItems.Table;
import factoryMathod.*;
import factoryMathod.creators.Creator;

import java.util.Scanner;

public class Run {

    public static void main(String[] args) {
        System.out.println("Starting program...");
        // run abstract factory
        goShoppingOfChosenStyleOfFurniture(createFurnitureFactoryOfChosenStyleOfFurniture(provideKindOfItem()));
        // run factory method
//        createTransportationPlanOfChosenKindOfVehicle();
    }

    // Demo of factory method
    private static void runTransportFactoryMethod(){
        createTransportationPlanOfChosenKindOfVehicle();
    }

    private static String provideKindOfItem(){
        Scanner s = new Scanner(System.in);
        System.out.println("Enter kind of ...");
        String kindOfItem = s.nextLine();
        System.out.println("Chosen kind: " + kindOfItem);
        s.close();
        return kindOfItem;
    }

    private static void createTransportationPlanOfChosenKindOfVehicle(){
        String kindOfTransport = provideKindOfItem();
        Logistics.generateLogistics(kindOfTransport).deliver();
    }

    // Demo of abstract factory for different style of furnitures and different items
    private static MainFurnitureFactory createFurnitureFactoryOfChosenStyleOfFurniture(String styleOfFurniture){
        MainFurnitureFactory furnitureFactoryInChosenStyle = StyledFurnitures.createFacoryMathodForChosenFurnitureStyle(styleOfFurniture);
        return furnitureFactoryInChosenStyle;
    }

    private static void goShoppingOfChosenStyleOfFurniture(MainFurnitureFactory furnitureFactoryInChosenStyle){
        Table table = furnitureFactoryInChosenStyle.createTable();
        Sofa sofa = furnitureFactoryInChosenStyle.createSofa();
        Armchair armchair = furnitureFactoryInChosenStyle.createArmchair();

        //actions
        table.calculateHowManyPeopleCanEatFromTable();
        sofa.calculateHowManyPeopleCanSit();
        armchair.showColourOfArmchair();
        armchair.showToughnessOfArmchair();
        armchair.showHowSoftFabricIs();
    }
}
