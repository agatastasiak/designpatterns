package builder.components;

public class Engine {

    private double engineVolume;
    private double acceleration;

    public Engine(double engineVolume, double acceleration) {
        this.engineVolume = engineVolume;
        this.acceleration = acceleration;
    }

    public double getEngineVolume() {
        return engineVolume;
    }

    public double getAcceleration() {
        return acceleration;
    }
}
