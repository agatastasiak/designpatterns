package builder.components;

public enum FamilyCarTypes {

    FAMILY("Family car");

    private String groupName;

    FamilyCarTypes(String groupName) {
        this.groupName = groupName;
    }

    public enum FamilyCarModels{

        SKODA("Fabia", "CITYGO"),
        FIAT("Punto");

        private String[] label;

        FamilyCarModels(String ... label) {
            this.label = label;
        }

        public String[] getLabel() {
            return label;
        }
    }

    public String getGroupName() {
        return groupName;
    }
}
