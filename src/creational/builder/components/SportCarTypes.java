package builder.components;

public enum SportCarTypes {

    SPORT("Sport car");

    private String groupName;

    SportCarTypes(String groupName) {
        this.groupName = groupName;
    }

    public enum SportCarModels{

        PORSCHE("Panamera"),
        JAGUAR("F-TYPE 1");

        private String label;

        SportCarModels(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }
    public String getGroupName() {
        return groupName;
    }
}
