package builder.builders;

import builder.products.Car;
import builder.components.*;

public class CarBuilder implements Builder {

    private Enum generalCarType;
    private int seats;
    private CarColours carColour;
    private Engine engine;
    private GearBox gearBox;
    private double price;
    private double trunkVolume;
    private boolean hasIsofix;
    private boolean hasAdditionalSuspension;

    @Override
    public void setGeneralCarType(Enum generalCarType) {
        this.generalCarType = generalCarType;
    }

    @Override
    public void setSeats(int seats) {
        this.seats = seats;
    }

    @Override
    public void setColour(CarColours carColour) {
        this.carColour = carColour;
    }

    @Override
    public void setGearBox(GearBox gearBox) {
        this.gearBox = gearBox;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void setTrunkVolume(double trunkVolume) {
        this.trunkVolume = trunkVolume;
    }

    @Override
    public void setHasIsofix(boolean hasIsofix) {
        this.hasIsofix = hasIsofix;
    }

    @Override
    public void setHasAdditionalSuspension(boolean hasAdditionalSuspension) {
        this.hasAdditionalSuspension = hasAdditionalSuspension;
    }

    @Override
    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    @Override
    public Car getProduct() {
        return new Car(generalCarType, seats, carColour, engine, gearBox, price, trunkVolume, hasIsofix,hasAdditionalSuspension);
    }
}
