package builder.builders;

import builder.products.Car;
import builder.components.*;

public interface Builder {

     void setGeneralCarType(Enum generalCarType);
     void setSeats(int seats);
     void setColour(CarColours carColour);
     void setEngine(Engine engine);
     void setGearBox(GearBox gearBox);
     void setPrice(double price);
     void setTrunkVolume(double trunkVolume);
     void setHasIsofix(boolean hasIsofix);
     void setHasAdditionalSuspension(boolean hasAdditionalSuspension);
     Car getProduct();
}
