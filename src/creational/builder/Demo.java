package builder;

import builder.builders.Builder;
import builder.builders.CarBuilder;
import builder.director.Director;
import builder.products.Car;

public class Demo {

    public static void main(String[] args) {

        CarBuilder carBuilder = new CarBuilder();
        Director director = new Director(carBuilder);
        director.produceSportCar();
        Car car = carBuilder.getProduct();
        director.presentCar(car);
    }
}
