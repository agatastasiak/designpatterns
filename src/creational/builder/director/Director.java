package builder.director;

import builder.products.Car;
import builder.builders.Builder;
import builder.components.*;

public class Director {

    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public void produceSportCar(){
        this.builder.setColour(CarColours.BLACK);
        this.builder.setEngine(new Engine(2.2,100.00));
        this.builder.setGearBox(new GearBox( 4));
        this.builder.setGeneralCarType(SportCarTypes.SportCarModels.JAGUAR);
        this.builder.setHasAdditionalSuspension(true);
        this.builder.setHasIsofix(false);
        this.builder.setTrunkVolume(1000);
        this.builder.setSeats(2);
        this.builder.setPrice(299.999);
    }

    public void presentCar(Car car){
        System.out.println("New car: " +
                car.getCarColour().toString() + " " +
                car.getEngine().toString() + " " +
                car.getPrice() + " zł " +
                car.getGeneralCarType() + " " +
                "isofix: " + car.isHasIsofix());
    }
}
