package builder.products;

import builder.components.*;

public class Car {

    private final Enum generalCarType;
    private final int seats;
    private final CarColours carColour;
    private final Engine engine;
    private final GearBox gearBox;
    private final double price;
    private final double trunkVolume;
    private final boolean hasIsofix;
    private final boolean hasAdditionalSuspension;

    public Car(Enum generalCarType, int seats, CarColours carColour, Engine engine, GearBox gearBox, double price, double trunkVolume, boolean hasIsofix, boolean hasAdditionalSuspension) {
        this.generalCarType = generalCarType;
        this.seats = seats;
        this.carColour = carColour;
        this.engine = engine;
        this.gearBox = gearBox;
        this.price = price;
        this.trunkVolume = trunkVolume;
        this.hasIsofix = hasIsofix;
        this.hasAdditionalSuspension = hasAdditionalSuspension;
    }

    public Enum getGeneralCarType() {
        return generalCarType;
    }

    public int getSeats() {
        return seats;
    }

    public CarColours getCarColour() {
        return carColour;
    }

    public Engine getEngine() {
        return engine;
    }

    public GearBox getGearBox() {
        return gearBox;
    }

    public double getPrice() {
        return price;
    }

    public double getTrunkVolume() {
        return trunkVolume;
    }

    public boolean isHasIsofix() {
        return hasIsofix;
    }

    public boolean isHasAdditionalSuspension() {
        return hasAdditionalSuspension;
    }
}
