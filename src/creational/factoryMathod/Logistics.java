package factoryMathod;

import factoryMathod.creators.AirLogistics;
import factoryMathod.creators.RoadLogistics;
import factoryMathod.creators.SeaLogistics;
import factoryMathod.product.Transport;

public class Logistics {

    public static Transport generateLogistics(String kindOfTransport){
        Transport transport = switch (kindOfTransport){
            case "Air" -> new AirLogistics().createTransport();
            case "Sea" -> new SeaLogistics().createTransport();
            case "Road" -> new RoadLogistics().createTransport();
            default -> throw new IllegalArgumentException("Unknown kind of transport: " + kindOfTransport);
        };
        return transport;
    }
}
