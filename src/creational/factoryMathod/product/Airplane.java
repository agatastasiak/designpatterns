package factoryMathod.product;

public class Airplane implements Transport {

    @Override
    public void deliver() {
        System.out.println("Delivered by Airplane");
    }

    @Override
    public int calculatePrice() {
        return 0;
    }
}
