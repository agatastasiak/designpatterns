package factoryMathod.product;

public class Ship implements Transport {

    @Override
    public void deliver() {
        System.out.println("Delivered by Ship");
    }

    @Override
    public int calculatePrice() {
        return 0;
    }
}
