package factoryMathod.product;

public interface Transport {

    void deliver();
    int calculatePrice();
}
