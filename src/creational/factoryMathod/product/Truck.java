package factoryMathod.product;

public class Truck implements Transport {

    @Override
    public void deliver() {
        System.out.println("Delivered by Truck");
    }

    @Override
    public int calculatePrice() {
        return 0;
    }
}
