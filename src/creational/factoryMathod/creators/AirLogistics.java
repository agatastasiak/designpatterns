package factoryMathod.creators;

import factoryMathod.product.Airplane;
import factoryMathod.product.Transport;

public class AirLogistics extends Creator {
    @Override
    public Transport createTransport() {
        return new Airplane();
    }
}
