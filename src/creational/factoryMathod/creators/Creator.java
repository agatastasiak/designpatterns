package factoryMathod.creators;

import factoryMathod.product.Transport;

public abstract class Creator {

    public abstract Transport createTransport();
}
