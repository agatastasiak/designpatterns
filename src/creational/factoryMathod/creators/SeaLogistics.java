package factoryMathod.creators;

import factoryMathod.product.Ship;
import factoryMathod.product.Transport;

public class SeaLogistics extends Creator {
    @Override
    public Transport createTransport() {
        return new Ship();
    }
}
