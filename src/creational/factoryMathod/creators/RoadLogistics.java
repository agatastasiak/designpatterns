package factoryMathod.creators;

import factoryMathod.product.Transport;
import factoryMathod.product.Truck;

public class RoadLogistics extends Creator {

    @Override
    public Transport createTransport() {
        return new Truck();
    }
}
