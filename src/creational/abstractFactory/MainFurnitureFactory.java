package abstractFactory;

import abstractFactory.furnitureMainItems.Armchair;
import abstractFactory.furnitureMainItems.Sofa;
import abstractFactory.furnitureMainItems.Table;
import abstractFactory.styleFurnitureFactory.ArtDecoFurnitureFactory;
import abstractFactory.styleFurnitureFactory.ModernFurnitureFactory;
import abstractFactory.styleFurnitureFactory.VictorianFurnitureFactory;

public interface MainFurnitureFactory {

    Table createTable();
    Sofa createSofa();
    Armchair createArmchair();

}
