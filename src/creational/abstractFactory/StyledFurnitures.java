package abstractFactory;

import abstractFactory.styleFurnitureFactory.ArtDecoFurnitureFactory;
import abstractFactory.styleFurnitureFactory.ModernFurnitureFactory;
import abstractFactory.styleFurnitureFactory.VictorianFurnitureFactory;

public class StyledFurnitures {

    public static MainFurnitureFactory createFacoryMathodForChosenFurnitureStyle(String furnitureStyle){
        MainFurnitureFactory furnitureFactory = switch (furnitureStyle){
            case "VICTORIAN" -> new VictorianFurnitureFactory();
            case "ARTDECO" -> new ArtDecoFurnitureFactory();
            case "MODERN" -> new ModernFurnitureFactory();
            default -> throw new IllegalArgumentException("Unknown style: " + furnitureStyle);
        };
        return furnitureFactory;
    }
}
