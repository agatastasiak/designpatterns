package abstractFactory.styleFurnitureFactory;

import abstractFactory.MainFurnitureFactory;
import abstractFactory.furnitureMainItems.Armchair;
import abstractFactory.furnitureMainItems.Sofa;
import abstractFactory.furnitureMainItems.Table;
import abstractFactory.furnitureStyles.artDeco.ArtDecoArmchair;
import abstractFactory.furnitureStyles.artDeco.ArtDecoSofa;
import abstractFactory.furnitureStyles.artDeco.ArtDecoTable;

public class ArtDecoFurnitureFactory implements MainFurnitureFactory {

    @Override
    public Table createTable() {
        return new ArtDecoTable();
    }

    @Override
    public Armchair createArmchair() {
        return new ArtDecoArmchair();
    }

    @Override
    public Sofa createSofa() {
        return new ArtDecoSofa();
    }
};
