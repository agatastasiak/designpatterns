package abstractFactory.styleFurnitureFactory;

import abstractFactory.MainFurnitureFactory;
import abstractFactory.furnitureMainItems.Armchair;
import abstractFactory.furnitureMainItems.Sofa;
import abstractFactory.furnitureMainItems.Table;
import abstractFactory.furnitureStyles.vicotrian.VictorianArmchair;
import abstractFactory.furnitureStyles.vicotrian.VictorianSofa;
import abstractFactory.furnitureStyles.vicotrian.VictorianTable;

public class VictorianFurnitureFactory implements MainFurnitureFactory {


    @Override
    public Table createTable() {
        return new VictorianTable();
    }

    @Override
    public Armchair createArmchair() {
        return new VictorianArmchair();
    }

    @Override
    public Sofa createSofa() {
        return new VictorianSofa();
    }
}
