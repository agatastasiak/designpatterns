package abstractFactory.styleFurnitureFactory;

import abstractFactory.MainFurnitureFactory;
import abstractFactory.furnitureMainItems.Armchair;
import abstractFactory.furnitureMainItems.Sofa;
import abstractFactory.furnitureMainItems.Table;
import abstractFactory.furnitureStyles.modern.ModernArmchair;
import abstractFactory.furnitureStyles.modern.ModernSofa;
import abstractFactory.furnitureStyles.modern.ModernTable;

public class ModernFurnitureFactory implements MainFurnitureFactory {

    @Override
    public Table createTable() {
        return new ModernTable();
    }

    @Override
    public Armchair createArmchair() {
        return new ModernArmchair();
    }

    @Override
    public Sofa createSofa() {
        return new ModernSofa();
    }
}
