package abstractFactory.furnitureMainItems;

import abstractFactory.Furniture;

public interface Armchair extends Furniture {

    void showToughnessOfArmchair();
    void showHowSoftFabricIs();
    void showColourOfArmchair();
}
