package abstractFactory.furnitureMainItems;

import abstractFactory.Furniture;

public interface Table extends Furniture {
    int calculateHowManyPeopleCanEatFromTable();
}
