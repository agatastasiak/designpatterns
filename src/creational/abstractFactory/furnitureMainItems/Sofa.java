package abstractFactory.furnitureMainItems;

import abstractFactory.Furniture;

public interface Sofa extends Furniture {
    int calculateHowManyPeopleCanSit();
}
