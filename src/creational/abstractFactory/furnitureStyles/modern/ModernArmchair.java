package abstractFactory.furnitureStyles.modern;

import abstractFactory.furnitureMainItems.Armchair;

public class ModernArmchair implements Armchair {
    @Override
    public void showToughnessOfArmchair() {
        System.out.println("Modern armchair");
    }

    @Override
    public void showHowSoftFabricIs() {
        System.out.println("Modern armchair");
    }

    @Override
    public void showColourOfArmchair() {
        System.out.println("Modern armchair");
    }
}
