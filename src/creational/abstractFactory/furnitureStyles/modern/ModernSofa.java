package abstractFactory.furnitureStyles.modern;

import abstractFactory.furnitureMainItems.Sofa;

public class ModernSofa implements Sofa {
    @Override
    public int calculateHowManyPeopleCanSit() {
        return 5;
    }
}
