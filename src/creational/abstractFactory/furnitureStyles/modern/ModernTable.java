package abstractFactory.furnitureStyles.modern;

import abstractFactory.furnitureMainItems.Table;

public class ModernTable implements Table {
    @Override
    public int calculateHowManyPeopleCanEatFromTable() {
        return 10;
    }
}
