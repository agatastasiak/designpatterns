package abstractFactory.furnitureStyles.vicotrian;

import abstractFactory.furnitureMainItems.Table;

public class VictorianTable implements Table {
    @Override
    public int calculateHowManyPeopleCanEatFromTable() {
        return 4;
    }
}
