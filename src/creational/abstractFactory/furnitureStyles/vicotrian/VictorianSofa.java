package abstractFactory.furnitureStyles.vicotrian;

import abstractFactory.furnitureMainItems.Sofa;

public class VictorianSofa implements Sofa {
    @Override
    public int calculateHowManyPeopleCanSit() {
        return 5;
    }
}
