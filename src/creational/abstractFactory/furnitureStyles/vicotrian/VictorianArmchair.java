package abstractFactory.furnitureStyles.vicotrian;

import abstractFactory.furnitureMainItems.Armchair;

public class VictorianArmchair implements Armchair {
    @Override
    public void showToughnessOfArmchair() {
        System.out.println("Victorian Armchair toughness");
    }

    @Override
    public void showHowSoftFabricIs() {
        System.out.println("Victorian VerySoft armchair");
    }

    @Override
    public void showColourOfArmchair() {
        System.out.println("Victorian Blue Armchair");
    }
}
