package abstractFactory.furnitureStyles.artDeco;

import abstractFactory.furnitureMainItems.Sofa;

public class ArtDecoSofa implements Sofa {
    @Override
    public int calculateHowManyPeopleCanSit() {
        return 2;
    }
}
