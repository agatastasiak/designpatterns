package abstractFactory.furnitureStyles.artDeco;

import abstractFactory.furnitureMainItems.Table;

public class ArtDecoTable implements Table {
    @Override
    public int calculateHowManyPeopleCanEatFromTable() {
        return 4;
    }
}
