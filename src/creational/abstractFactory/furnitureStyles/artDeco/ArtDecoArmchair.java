package abstractFactory.furnitureStyles.artDeco;

import abstractFactory.furnitureMainItems.Armchair;

public class ArtDecoArmchair implements Armchair {

    @Override
    public void showToughnessOfArmchair() {
        System.out.println("ArtDecoArmchair It's very comfortable armchair");
    }

    @Override
    public void showHowSoftFabricIs() {
        System.out.println("ArtDecoArmchair And very soft");
    }

    @Override
    public void showColourOfArmchair() {
        System.out.println("ArtDecoArmchair Whatever, colour doesn't matter");
    }
}
